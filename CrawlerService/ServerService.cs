﻿using CrawlerDAL;
using CrawlerShared;
using CrawlerShared.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading;

namespace CrawlerService
{
    public class ServerService
    {

        #region GET CONFIG FILE
        /// <summary>
        /// GET CONFIG FILE
        /// </summary>
        public Tuple<JObject, string> getConfig() {
            string basedirectory = AppDomain.CurrentDomain.BaseDirectory;
            string configfilepath = basedirectory + "appsettings.json";
            string jsondata = File.ReadAllText(configfilepath);
            var ConfData = JObject.Parse(jsondata);
            int ServerId = Convert.ToInt32(ConfData["ServerId"]);
            var ConfFilePath = ConfData["ConfigFilePath"].ToString();
            JObject JsonOutPut = null;
            string ServerStatus = null;
            SqlParameter[] parms ={
                    new SqlParameter("@vmId",ServerId),
                 };

            DataTable dt = DAL.GenerateDataTable("USP_AdsCrawlerGetSearchConfig", CommandType.StoredProcedure, parms);
            if (dt.Rows.Count > 0 && dt.Rows[0]["serverStatus"].ToString() == "Server is busy ..!")
            {
                var chcekFile = ConfData["ReadJsonDataPath"].ToString();

                if (File.Exists(chcekFile + "completed.txt"))
                {
                    var info = new FileInfo(chcekFile + "completed.txt");
                    if (info.Length != 0)
                    {
                        File.WriteAllText(chcekFile + "completed.txt", "");
                        bool _insertdata = readJsonAndInsertData();
                        if (_insertdata)
                        {
                            ServerStatus = "Data Inserted And Server will be free for next run";
                            Console.WriteLine("Data inserted successfully,Task Completed.");
                            WriteToFile("Data inserted successfully,Task Completed.");
                        }
                    }
                    else
                    {
                        WriteToFile("Processed data alredy inserted.");
                        Console.WriteLine("Processed data alredy inserted.");
                    }
                }
                else
                {
                    ServerStatus = dt.Rows[0]["serverStatus"].ToString();
                    WriteToFile("Please check process log completed.txt not available. ! Task is in running state or new task will be start. ");
                    Console.WriteLine("Please check process log .completed.txt not available. ! Task is in running state or new task will be start. ");
                }
            }
            else
            {
                if (dt.Rows.Count > 0 && dt.Rows[0]["finalConf"].ToString() != "")
                {
                    try
                    {
                        var json = dt.Rows[0]["finalConf"].ToString();
                        JObject _res = JObject.Parse(json);
                        JObject channel = (JObject)_res;
                        JsonOutPut = _res;
                        File.WriteAllText(ConfFilePath, _res.ToString());
                        Console.WriteLine("Conf.Json Created.");
                        WriteToFile("Conf.Json Created.");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Please Check Error Occure While generating Conf.Json");
                        WriteToFile("Please Check Proplem Occure While generating Conf.Json");
                        throw ex;
                    }
                }
                else
                {
                    if (dt.Rows[0]["finalConf"].ToString() == "")
                    {
                        ServerStatus = "Task Not Available For Run.";
                    }
                    else
                    {
                        ServerStatus = dt.Rows[0]["serverStatus"].ToString();
                        Console.WriteLine("Task is in running state Or Task not available for run. Please check server and task status..!");
                        WriteToFile("Task is in running state Or Task not available for run. Please check server and task status..!");
                    }
                }
            }

            
            return Tuple.Create(JsonOutPut, ServerStatus);
        }

        #endregion

        #region Automation Script start on Server
        /// <summary>
        /// Automation Script start on Server
        /// </summary>
        public void StartCrawlerOnServer()
        {
            try
            {
                string basedirectory = AppDomain.CurrentDomain.BaseDirectory;
                string configfilepath = basedirectory + "appsettings.json";
                string jsondata = File.ReadAllText(configfilepath);
                var data1 = JObject.Parse(jsondata);
                string cmd1 = Convert.ToString(data1["ScanCmdOne"]);
                string cmd2 = Convert.ToString(data1["ScanCmdTwo"]);
                string cmd3 = Convert.ToString(data1["ScanCmdThree"]);
                StartCommandWithCmd(cmd1, cmd2, cmd3);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Start script
        /// <summary>
        /// Start script
        /// </summary>
        /// <param name="cmd1"></param>
        /// <param name="cmd2"></param>
        /// <param name="cmd3"></param>
        public void StartCommandWithCmd(string cmd1, string cmd2, string cmd3)
        {
            try
            {
                Process process = new Process();
                ProcessStartInfo psi = new ProcessStartInfo()
                {
                    FileName = "cmd.exe",
                    CreateNoWindow = true,
                    RedirectStandardOutput = true,
                    RedirectStandardInput = true,
                    UseShellExecute = false
                };
                process.StartInfo = psi;
                process.Start();
                process.StandardInput.WriteLine();
                using (StreamWriter sw = process.StandardInput)
                {
                    if (sw.BaseStream.CanWrite)
                    {
                        sw.WriteLine(cmd1);
                        sw.WriteLine(cmd2);
                        sw.WriteLine(cmd3);
                    }
                }
                StreamReader ischkout = process.StandardOutput;
                Thread.Sleep(3000);
               

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region Use for insert Data
        /// <summary>
        /// read Json And Insert Data
        /// </summary>
        /// <returns></returns>
        public bool readJsonAndInsertData()
        {
            bool result = false;
            try
            {
                string basedirectory = AppDomain.CurrentDomain.BaseDirectory;
                string configfilepath = basedirectory + "appsettings.json";
                string jsondata = File.ReadAllText(configfilepath);
                var ConfData = JObject.Parse(jsondata);
                var chcekFile = ConfData["ReadJsonDataPath"].ToString();

                if (File.Exists(chcekFile + "depth0.json"))
                {
                    string Data = File.ReadAllText(chcekFile + "depth0.json");
                    var JsonData = JArray.Parse(Data);
                    if (JsonData.Count > 0)
                    {
                        SqlParameter[] parm ={
                            new SqlParameter("@seedObject",Data),
                        };
                        DataTable _Datadt = DAL.GenerateDataTable("USP_AdsCrawlerSearchEngineData", CommandType.StoredProcedure, parm);

                        if (_Datadt.Rows.Count > 0)
                        {
                             File.Delete(chcekFile + "completed.txt");
                             int TaskId = Convert.ToInt32(_Datadt.Rows[0]["TaskId"]);
                             UpdateTaskStatusSearch(TaskId);
                             result = true;
                        }
                        else
                        {
                            Console.WriteLine("Batch Data inserted successfully,Batch Completed.");
                            WriteToFile("Batch Data inserted successfully,Batch Completed.");
                        }
                    }
                    else
                    {
                        WriteToFile("Error..! Please check output file .");
                        Console.WriteLine("Error..! Please check output file .");
                    }
                }
                else
                {
                    WriteToFile("Output.json not available.");
                    Console.WriteLine("Output.json not available.");
                }
            }
            catch (Exception ex)
            {
                WriteToFile("Please chekc error. While inserting Data.");
                Console.WriteLine("Please chekc error. While inserting Data.");
                WriteToFile("Exception message : " + ex.Message);
                Console.WriteLine("Exception message : " + ex.Message);
                throw ex;
            }
            return result;
        }
        #endregion

        #region Write To File
        /// <summary>
        /// Write To File
        /// </summary>
        /// <param name="text"></param>
        public static void WriteToFile(string text)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "processlog/process.txt";
            if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "processlog"))
                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "processlog");
            using (StreamWriter writer = new StreamWriter(path, true))
            {
                writer.WriteLine(DateTime.Now.ToString() + " : " + text);
                writer.Close();
            }
        }
        #endregion

        #region update task status for search crawler
        /// <summary>
        /// update task status for search crawler
        /// </summary>
        /// <param name="JsonObject"></param>
        /// <returns></returns>
        public bool UpdateTaskStatusSearch(int taskId)
        {
            bool result = false;
            try
            {
                SqlParameter[] param ={
                  new SqlParameter("@taskId",taskId),
            };
                DataTable dt = DAL.GenerateDataTable("USP_AdsCrawlerSearchTaskUpdate", CommandType.StoredProcedure, param);
                if (dt.Rows.Count > 0)
                {
                    string message = dt.Rows[0]["Message"].ToString();
                    if (message == "InputNotAvailableForInternalDepth")
                    {
                        result = true;
                    }
                    else
                    {
                        if (message == "InternalDepthWillRun")
                        {
                            result = true;
                        }
                        else
                        {
                            int TaskId = Convert.ToInt32(dt.Rows[0]["TaskId"]);
                            GetCompleteTaskDetails(TaskId);
                            result = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }
        #endregion

        #region Get Complete Task Details
        /// <summary>
        /// Get Complete Task Details
        /// </summary>
        /// <param name="TaskId"></param>
        public void GetCompleteTaskDetails(int TaskId)
        {
            try
            {
                string basedirectory = AppDomain.CurrentDomain.BaseDirectory;
                string configfilepath = basedirectory + "appsettings.json";
                string jsondata = File.ReadAllText(configfilepath);
                var _config = JObject.Parse(jsondata);
                AdsCrawlerTaskCompleteModel adsCrawler = new AdsCrawlerTaskCompleteModel();

                SqlParameter[] param_InsertMain ={
                 new SqlParameter("@taskId",TaskId),
                };

                Boolean dtInsertResult = DAL.ExecuteNonQuery("USP_AdsCrawlerTransferData", CommandType.StoredProcedure, param_InsertMain);

                if (dtInsertResult)
                {
                    SqlParameter[] param1 ={
                        new SqlParameter("@taskId",TaskId),
                    };

                    SqlParameter[] param01 ={
                        new SqlParameter("@taskId",TaskId),
                    };

                    DataSet dsResult = DAL.GenerateDataSet("USP_AdsCrawlerEmailReport", CommandType.StoredProcedure, param1);
                    DataSet dsExResult = DAL.GenerateDataSet("USP_AdsCrawlerExceptionEmailReport", CommandType.StoredProcedure, param01);
                    string reportFilePath = Convert.ToString(_config["AdsCrawlerReportFile"]);
                    string exceptionFilePath = Convert.ToString(_config["AdsCrawlerExceptionFile"]);
                    if (!Directory.Exists(reportFilePath))
                    {
                        Directory.CreateDirectory(reportFilePath);
                    }
                    createListToTable.DeleteExcelFiles(reportFilePath);
                    string originalFilePath = createListToTable.CreateXLSXFile(dsResult, reportFilePath, "_Report.xlsx");
                    WriteToFile("Created Report XLSX File.");
                    Console.WriteLine("Created Report XLSX File.");

                    if (!Directory.Exists(exceptionFilePath))
                    {
                        Directory.CreateDirectory(exceptionFilePath);
                    }
                    createListToTable.DeleteExcelFiles(exceptionFilePath);
                    string originalExFilePath = createListToTable.CreateXLSXFile(dsExResult, exceptionFilePath, "Exception.xlsx");
                    WriteToFile("Created Exception Report XLSX File.");
                    Console.WriteLine("Created Exception Report XLSX File.");


                    SqlParameter[] param ={
                        new SqlParameter("@taskId",TaskId),
                    };

                    DataTable dt = DAL.GenerateDataTable("USP_AdsCrawlerTaskCompleteNotificationEmail", CommandType.StoredProcedure, param);
                    if (dt.Rows.Count > 0)
                    {
                        WriteToFile("Send Email Details Fetch Successfully");
                        Console.WriteLine("Send Email Details Fetch Successfully");
                        List<AdsCrawlerTaskCompleteModel> list = createListToTable.DataTableToMultipleList<AdsCrawlerTaskCompleteModel>(dt);
                        adsCrawler.projectName = list.Select(w => w.projectName).FirstOrDefault();
                        adsCrawler.createdDate = Convert.ToDateTime(list.Select(w => w.createdDate).FirstOrDefault());
                        adsCrawler.completedDate = Convert.ToDateTime(list.Select(w => w.completedDate).FirstOrDefault());
                        adsCrawler.notifyName = list.Select(w => w.notifyName).FirstOrDefault();
                        adsCrawler.notifyEmails = list.Select(w => w.notifyEmails).FirstOrDefault();
                        adsCrawler.notifyCCEmails = list.Select(w => w.notifyCCEmails).FirstOrDefault();
                        ComposeEmail(adsCrawler, originalFilePath,"");
                        Thread.Sleep(4000);
                        if (dsExResult.Tables[0].Rows.Count > 0)
                        {
                            ComposeEmail(adsCrawler, originalExFilePath, "ExceptionEmail");
                        }
                    }

                }

                    
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region Compose Email Notification
        /// <summary>
        /// Compose Email Notification
        /// </summary>
        /// <param name="adsCrawlerEmail"></param>
        /// <param name="emailDetails"></param>
        /// <returns></returns>
        private void ComposeEmail(AdsCrawlerTaskCompleteModel adsCrawler, string originalFilePath,string emailType)
        {
            if (emailType == "ExceptionEmail")
            {
                try
                {
                    string basedirectory = AppDomain.CurrentDomain.BaseDirectory;
                    string configfilepath = basedirectory + "appsettings.json";
                    string jsondata = File.ReadAllText(configfilepath);
                    var _config = JObject.Parse(jsondata);
                    List<string> EmptyList = new List<string>();
                    List<string> receiverList = new List<string>() { adsCrawler.notifyEmails };
                    //string templatePath = "";
                    string mailSubject = "Paid Ads Crawler || SERP Exception While Running Project  " + adsCrawler.projectName + '_' + DateTime.Now.ToShortDateString();
                    List<string> ListmailTO = new List<string>();
                    List<string> ListmailCC = new List<string>();
                    List<string> ListmailBCC = new List<string>();
                    var body = "";
                    ListmailTO = new List<string>() { adsCrawler.notifyEmails };
                    ListmailCC = new List<string>() { adsCrawler.notifyCCEmails };
                    string operationsHtml = string.Empty;
                    //emailDetails.operations.ForEach(el => {
                    //    operationsHtml = operationsHtml + $"<li>{el}</li>";
                    //});
                    //operationsHtml = $"<ul>{operationsHtml}</ul>";
                    //using (StreamReader reader = new StreamReader(templatePath))
                    //{
                    //    body = reader.ReadToEnd();
                    //}
                    //body = body.Replace("[###User_name###]", adsCrawler.notifyName);
                    //body = body.Replace("[###Project_name###]", adsCrawler.projectName);
                    //body = body.Replace("[###Start_Date###]", Convert.ToString(adsCrawler.createdDate));
                    //body = body.Replace("[###Complete_Date###]", Convert.ToString(adsCrawler.completedDate));
                    if (ListmailTO.Count > 0)
                    {
                        SendEmail(ListmailTO, mailSubject, GetAlternateView(body), ListmailCC, ListmailBCC, "", originalFilePath, emailType);
                    }
                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }
            else {
                try
                {
                    string basedirectory = AppDomain.CurrentDomain.BaseDirectory;
                    string configfilepath = basedirectory + "appsettings.json";
                    string jsondata = File.ReadAllText(configfilepath);
                    var _config = JObject.Parse(jsondata);
                    List<string> EmptyList = new List<string>();
                    List<string> receiverList = new List<string>() { adsCrawler.notifyEmails };
                    string templatePath = Convert.ToString(_config["CompleteTaskTemplate"]);
                    string mailSubject = "Paid Ads Crawler || Task Completed " + adsCrawler.projectName + '_' + DateTime.Now.ToShortDateString();
                    List<string> ListmailTO = new List<string>();
                    List<string> ListmailCC = new List<string>();
                    List<string> ListmailBCC = new List<string>();
                    var body = "";
                    ListmailTO = new List<string>() { adsCrawler.notifyEmails };
                    ListmailCC = new List<string>() { adsCrawler.notifyCCEmails };
                    string operationsHtml = string.Empty;
                    //emailDetails.operations.ForEach(el => {
                    //    operationsHtml = operationsHtml + $"<li>{el}</li>";
                    //});
                    //operationsHtml = $"<ul>{operationsHtml}</ul>";
                    using (StreamReader reader = new StreamReader(templatePath))
                    {
                        body = reader.ReadToEnd();
                    }
                    body = body.Replace("[###User_name###]", adsCrawler.notifyName);
                    body = body.Replace("[###Project_name###]", adsCrawler.projectName);
                    body = body.Replace("[###Start_Date###]", Convert.ToString(adsCrawler.createdDate));
                    body = body.Replace("[###Complete_Date###]", Convert.ToString(adsCrawler.completedDate));
                    if (ListmailTO.Count > 0)
                    {
                        SendEmail(ListmailTO, mailSubject, GetAlternateView(body), ListmailCC, ListmailBCC, "", originalFilePath,"");
                    }
                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }     
        }
        #endregion

        #region Get Alternate View
        /// <summary>
        /// Get Alternate View
        /// </summary>
        /// <param name="body"></param>
        /// <returns></returns>
        private static AlternateView GetAlternateView(string body)
        {
            AlternateView alternateView = AlternateView.CreateAlternateViewFromString(body, null, System.Net.Mime.MediaTypeNames.Text.Html);
            return alternateView;
        }
        #endregion

        #region Send Email
        /// <summary>
        /// Send Email
        /// </summary>
        /// <param name="toAddress"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="mailCC"></param>
        /// <param name="mailBCC"></param>
        /// <param name="PFAfilePath"></param>
        public void SendEmail(List<string> toAddress, string subject, AlternateView body, List<string> mailCC, List<string> mailBCC, string PFAfilePath, string originalFilePath,string emailType)
        {
            string basedirectory = AppDomain.CurrentDomain.BaseDirectory;
            string configfilepath = basedirectory + "appsettings.json";
            string jsondata = File.ReadAllText(configfilepath);
            var _config = JObject.Parse(jsondata);
            var userCredentials = new System.Net.NetworkCredential(Convert.ToString(_config["SMTPUserName"]), Convert.ToString(_config["SMTPPassword"]));

            if (Convert.ToBoolean(_config["EmailAlertEnabled"]))
            {
                SmtpClient smtp = new SmtpClient
                {
                    Host = Convert.ToString(_config["SMTPHost"]),
                    Port = Convert.ToInt32(_config["SMTPPort"]),
                    EnableSsl = Convert.ToBoolean(_config["SMTPEnableSsl"]),
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Timeout = Convert.ToInt32(_config["SMTPTimeout"])
                };

                smtp.Credentials = userCredentials;
                MailMessage message = new MailMessage();
                message.From = new MailAddress(Convert.ToString(_config["SenderEmailAddress"]), Convert.ToString(_config["SenderDisplayName"]));
                message.Subject = subject;
                message.AlternateViews.Add(body);
                message.IsBodyHtml = true;

                System.Net.Mail.Attachment attachment;
                attachment = new System.Net.Mail.Attachment(originalFilePath);
                message.Attachments.Add(attachment);

                if (toAddress.Count > 0 && !string.IsNullOrEmpty(toAddress[0]))
                {
                    toAddress.ForEach(a =>
                    {
                        message.To.Add(a);
                    });
                }
                if (mailCC.Count > 0 && !string.IsNullOrEmpty(mailCC[0]))
                {
                    mailCC.ForEach(a =>
                    {
                        message.CC.Add(a);
                    });
                }

                if (mailBCC.Count > 0 && !string.IsNullOrEmpty(mailBCC[0]))
                {
                    mailBCC.ForEach(a =>
                    {
                        message.Bcc.Add(a);
                    });
                }


                if (!string.IsNullOrEmpty(PFAfilePath))
                {
                    message.Attachments.Add(new Attachment(PFAfilePath));
                }
                smtp.Send(message);
            }
        }
        #endregion
    }
}
