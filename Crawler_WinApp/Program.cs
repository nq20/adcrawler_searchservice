﻿using CrawlerService;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Threading;

namespace Crawler_WinApp
{
    class Program
    {
        static void Main()
        {
            try
            {
                    ServerService server = new ServerService();
                    Console.WriteLine("process about to start..!");
                    WriteToFile("process about to start..!");
                    var _data = server.getConfig();
                    Thread.Sleep(3000);
                if (_data.Item2 != null)
                {
                    if (_data.Item2.ToString() == "Data Inserted And Server will be free for next run")
                    {
                        Console.WriteLine("Data Inserted And Server will be free for next run");
                        WriteToFile("Data Inserted And Server will be free for next run");
                    }
                    else if (_data.Item2.ToString() == "Task Not Available For Run.")
                    {
                        Console.WriteLine("Task Not Available For Run. Server Is Free");
                        WriteToFile("Task Not Available For Run.Server Is Free");
                    }
                    else {
                        Console.WriteLine("Server is busy..! ");
                        WriteToFile("Server is busy..! ");
                    }  
                }
                else {
                    if (_data.Item1 != null)
                    {
                        try
                        {
                            server.StartCrawlerOnServer();
                            Console.WriteLine("Automation Script Started..! TaskId : " + _data.Item1["taskId"]);
                            WriteToFile("Automation Script Started..! TaskId : " + _data.Item1["taskId"]);
                        }
                        catch {
                            Console.WriteLine("Error while starting script ..!");
                            WriteToFile("Error while starting script ..!");
                        }
                        
                    }
                    else
                    {
                        Console.WriteLine("Task Is Running Or Server Not Updated Please Check ..! ");
                        WriteToFile("Task Is Running Or Server Not Updated Please Check ..! ");
                    }
                }
   
            }
            catch(Exception ex) {
                Exceptionlog(ex);
            }
        }

        #region USE FOR  Write To File process txt
        /// <summary>
        /// USE FOR  Write To File process txt
        /// </summary>
        /// <param name="text"></param>
        private static void WriteToFile(string text)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "processlog/process.txt";
            if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "processlog"))
                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "processlog");
            //if (!File.Exists(AppDomain.CurrentDomain.BaseDirectory + "processlog/process.txt"))
            // File.Create(AppDomain.CurrentDomain.BaseDirectory + "processlog/process.txt");
            using (StreamWriter writer = new StreamWriter(path, true))
            {
                writer.WriteLine(DateTime.Now.ToString() + " : " + text);
                writer.Close();
            }
        }
        #endregion

        #region USE FOR WRITE Exception log
        /// <summary>
        /// USE FOR WRITE Exception log
        /// </summary>
        /// <param name="ex"></param>
        private static void Exceptionlog(Exception ex)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "errorlog/error_" + DateTime.Now.Date.ToLongDateString() + ".txt";
            if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "errorlog"))
                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "errorlog");
            //if (!File.Exists(path))
            // File.Create(path);
            using (StreamWriter writer = new StreamWriter(path, true))
            {
                writer.WriteLine(DateTime.Now.ToString() + "ErrorMessage : " + ex.Message + "\n Tracing :" + ex.StackTrace);
                writer.Close();
            }
        }
        #endregion
    }
}
