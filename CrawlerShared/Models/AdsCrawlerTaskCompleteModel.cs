﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CrawlerShared.Models
{
    public class AdsCrawlerTaskCompleteModel
    {
        public string projectName { get; set; }
        public DateTime createdDate { get; set; }
        public DateTime completedDate { get; set; }
        public string notifyName { get; set; }
        public string notifyEmails { get; set; }
        public string notifyCCEmails { get; set; }
    }
}
