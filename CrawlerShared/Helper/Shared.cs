﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CrawlerShared
{
    public static class createListToTable
    {
        #region Get DataTable To MultipleList
        /// <summary>
        /// Get DataTable To MultipleList
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="table"></param>
        /// <returns></returns>
        public static List<T> DataTableToMultipleList<T>(this DataTable table) where T : new()
        {
            List<T> list = new List<T>();
            var typeProperties = typeof(T).GetProperties().Select(propertyInfo => new
            {
                PropertyInfo = propertyInfo,
                Type = Nullable.GetUnderlyingType(propertyInfo.PropertyType) ?? propertyInfo.PropertyType
            }).ToList();

            foreach (var row in table.Rows.Cast<DataRow>())
            {
                T obj = new T();
                foreach (var typeProperty in typeProperties)
                {
                    object value = row[typeProperty.PropertyInfo.Name];
                    object safeValue = value == null || DBNull.Value.Equals(value)
                        ? null
                        : Convert.ChangeType(value, typeProperty.Type);

                    typeProperty.PropertyInfo.SetValue(obj, safeValue, null);
                }
                list.Add(obj);
            }
            return list;
        }
        #endregion

        #region Get DataTable to SingleList
        /// <summary>
        /// Get DataTable to SingleList
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="table"></param>
        /// <returns></returns>
        public static T DataTableToSingList<T>(this DataTable table) where T : new()
        {
            List<T> list = new List<T>();
            var typeProperties = typeof(T).GetProperties().Select(propertyInfo => new
            {
                PropertyInfo = propertyInfo,
                Type = Nullable.GetUnderlyingType(propertyInfo.PropertyType) ?? propertyInfo.PropertyType
            }).ToList();

            foreach (var row in table.Rows.Cast<DataRow>())
            {
                T obj = new T();
                foreach (var typeProperty in typeProperties)
                {
                    object value = row[typeProperty.PropertyInfo.Name];
                    object safeValue = value == null || DBNull.Value.Equals(value)
                        ? null
                        : Convert.ChangeType(value, typeProperty.Type);

                    typeProperty.PropertyInfo.SetValue(obj, safeValue, null);
                }

                return obj;
            }
            return new T();
        }
        #endregion

        #region List To Data table
        /// <summary>
        /// List To Data table
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <returns></returns>
        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        public static DataTable ConvertListToDataTable(List<string> list)
        {
            // New table.
            DataTable table = new DataTable();
            table.Columns.Add();
            // Add rows.
            foreach (var array in list)
            {
                table.Rows.Add(array);
            }
            return table;
        }

        #endregion

        #region Use Create XLSX File
        /// <summary>
        /// Create XLSX File
        /// </summary>
        /// <param name="dataSets"></param>
        /// <param name="strFilePath"></param>
        public static string CreateXLSXFile(DataSet dataSets, string strFilePath,string strFileName)
        {
            var fileName="";
            if (strFileName == "_Report.xlsx")
            {
                var sheetNames = new List<string>() { "Direct Links", "Others" };
                Guid myUUId = Guid.NewGuid();
                string convertedUUID = myUUId.ToString();
                fileName = convertedUUID + strFileName;
                using (XLWorkbook wbook = new XLWorkbook())
                {
                    for (int k = 0; k < dataSets.Tables.Count; k++)
                    {
                        DataTable dt = dataSets.Tables[k];
                        IXLWorksheet Sheet = wbook.Worksheets.Add(sheetNames[k]);
                        for (int i = 0; i < dt.Columns.Count; i++)
                        {
                            Sheet.Cell(1, (i + 1)).Value = dt.Columns[i].ColumnName;
                        }
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            for (int j = 0; j < dt.Columns.Count; j++)
                            {
                                Sheet.Cell((i + 2), (j + 1)).Value = dt.Rows[i][j].ToString();
                            }
                        }
                    }
                    using (MemoryStream exelStream = new MemoryStream())
                    {

                        FileStream file = new FileStream(Path.Combine(strFilePath, fileName), FileMode.OpenOrCreate);
                        wbook.SaveAs(exelStream);
                        exelStream.WriteTo(file);
                        file.Close();
                        exelStream.Close();
                        wbook.Dispose();
                    }
                }
            }
            else {

                var sheetNames = new List<string>() { "Exception List"};
                Guid myUUId = Guid.NewGuid();
                string convertedUUID = myUUId.ToString();
                fileName = convertedUUID + strFileName;
                using (XLWorkbook wbook = new XLWorkbook())
                {
                    for (int k = 0; k < dataSets.Tables.Count; k++)
                    {
                        DataTable dt = dataSets.Tables[k];
                        IXLWorksheet Sheet = wbook.Worksheets.Add(sheetNames[k]);
                        for (int i = 0; i < dt.Columns.Count; i++)
                        {
                            Sheet.Cell(1, (i + 1)).Value = dt.Columns[i].ColumnName;
                        }
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            for (int j = 0; j < dt.Columns.Count; j++)
                            {
                                Sheet.Cell((i + 2), (j + 1)).Value = dt.Rows[i][j].ToString();
                            }
                        }
                    }
                    using (MemoryStream exelStream = new MemoryStream())
                    {

                        FileStream file = new FileStream(Path.Combine(strFilePath, fileName), FileMode.OpenOrCreate);
                        wbook.SaveAs(exelStream);
                        exelStream.WriteTo(file);
                        file.Close();
                        exelStream.Close();
                        wbook.Dispose();
                    }
                }
            }
            
            return Path.Combine(strFilePath, fileName);
        }
        #endregion

        #region Use For Delete Excel Files
        /// <summary>
        /// Use For Delete Excel Files
        /// </summary>
        /// <param name="strFilePath"></param>
        public static void DeleteExcelFiles(string strFilePath)
        {
            System.IO.DirectoryInfo di = new DirectoryInfo(strFilePath);

            foreach (FileInfo file in di.GetFiles())
            {
                try
                {
                    System.GC.Collect();
                    System.GC.WaitForPendingFinalizers();
                    file.IsReadOnly = false;
                    file.Delete();
                }
                catch
                {
                    continue;
                }
            }
        }
        #endregion
    }

}
