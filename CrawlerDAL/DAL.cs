﻿using Newtonsoft.Json.Linq;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace CrawlerDAL
{
    public class DAL
    {
        public static string connectionString;

       

        public DAL() { }
        /// <summary>
        /// Returns the type of data provider that the application is using. The application will read from it's config file in the Provider element
        /// of the AppSettings node.
        /// </summary>
        /// <returns></returns>
        private static string GetProviders()
        {
            string basedirectory = AppDomain.CurrentDomain.BaseDirectory;
            string configfilepath = basedirectory + "appsettings.json";
            string jsondata = File.ReadAllText(configfilepath);
            var data1 = JObject.Parse(jsondata);
            string connectionstring = data1["ConnectionStrings"]["DatabaseConnection"].ToString();
            string provider = connectionstring;
            return provider;
        }

        /// <summary>
        /// Returns a DataTable object for a specific SQL statement or stored procedure.
        /// </summary>
        /// <param name="strSPName"></param>
        /// <param name="cmdTypeName"></param>
        /// <param name="arrlstParameters"></param>
        /// <returns></returns>
        public static DataTable GenerateDataTable(string strSPName, CommandType cmdTypeName, SqlParameter[] pSQLParameters)
        {
            DataTable objDataTable = new DataTable();
            SqlDataAdapter objSqlDataAdapter = null;
            SqlConnection Conn = null;
            SqlCommand objSQLCommand = null;
            try
            {
                Conn = new SqlConnection(GetProviders());
                Conn.Open();
                objSQLCommand = new SqlCommand();
                objSQLCommand.CommandType = cmdTypeName;
                objSQLCommand.CommandText = strSPName;
                objSQLCommand.Connection = Conn;
                objSQLCommand.CommandTimeout = 0;

                if (pSQLParameters != null)
                {
                    foreach (SqlParameter param in pSQLParameters)
                    {
                        objSQLCommand.Parameters.Add(param);
                    }
                }

                objSqlDataAdapter = new SqlDataAdapter(objSQLCommand);
                objSqlDataAdapter.Fill(objDataTable);

            }
            finally
            {
                if (objSqlDataAdapter != null)
                {
                    objSqlDataAdapter.Dispose();
                }
                if (objSQLCommand != null)
                {
                    objSQLCommand.Dispose();
                }
                if (Conn != null)
                {
                    if (Conn.State == ConnectionState.Open)
                    {
                        //ConnStr.Close();
                        Conn.Dispose();
                    }
                }

            }
            return objDataTable;
        }
        /// <summary>
        /// Returns a DataSet object for a specific SQL statement or stored procedure.
        /// </summary>
        /// <param name="strSPName"></param>
        /// <param name="cmdTypeName"></param>
        /// <param name="arrlstParameters"></param>
        /// <returns></returns>
        public static DataSet GenerateDataSet(string strSPName, CommandType cmdTypeName, SqlParameter[] pSQLParameters)
        {
            DataSet objDataSet = new DataSet();
            SqlDataAdapter objSqlDataAdapter = null;
            SqlConnection Conn = null;
            SqlCommand objSQLCommand = null;
            try
            {
                Conn = new SqlConnection(GetProviders());
                Conn.Open();
                objSQLCommand = new SqlCommand();
                objSQLCommand.CommandType = cmdTypeName;
                objSQLCommand.CommandText = strSPName;
                objSQLCommand.Connection = Conn;
                objSQLCommand.CommandTimeout = 0;

                if (pSQLParameters != null)
                {
                    foreach (SqlParameter param in pSQLParameters)
                    {
                        objSQLCommand.Parameters.Add(param);
                    }
                }
                objSqlDataAdapter = new SqlDataAdapter(objSQLCommand);
                objSqlDataAdapter.Fill(objDataSet);

            }
            finally
            {
                if (objSqlDataAdapter != null)
                {
                    objSqlDataAdapter.Dispose();
                }
                if (objSQLCommand != null)
                {
                    objSQLCommand.Dispose();
                }
                if (Conn != null)
                {
                    if (Conn.State == ConnectionState.Open)
                    {
                        //ConnStr.Close();
                        Conn.Dispose();
                    }
                }

            }
            return objDataSet;
        }
        /// <summary>
        /// Executes a SQL statement or stored procedure that returns a single scalar value.
        /// </summary>
        /// <param name="strSPName"></param>
        /// <param name="cmdTypeName"></param>
        /// <param name="arrlstParameters"></param>
        /// <returns></returns>
        public static object GenerateScalar(string strSPName, CommandType cmdTypeName, SqlParameter[] pSQLParameters)
        {
            object objReturnValue = null;
            SqlCommand objSQLCommand = new SqlCommand();
            SqlConnection Conn = null;
            try
            {
                Conn = new SqlConnection(GetProviders());
                Conn.Open();
                objSQLCommand.CommandType = cmdTypeName;
                objSQLCommand.CommandText = strSPName;
                objSQLCommand.Connection = Conn;

                if (pSQLParameters != null)
                {
                    foreach (SqlParameter param in pSQLParameters)
                    {
                        objSQLCommand.Parameters.Add(param);
                    }
                }
                objReturnValue = objSQLCommand.ExecuteScalar();

            }
            finally
            {
                if (objSQLCommand != null)
                {
                    objSQLCommand.Dispose();
                }
                if (Conn != null)
                {
                    if (Conn.State == ConnectionState.Open)
                    {
                        //ConnStr.Close();
                        Conn.Dispose();
                    }
                }
            }
            return objReturnValue;
        }

        /// <summary>
        /// Returns a DataReader object for a specific SQL statement or stored procedure.
        /// </summary>
        /// <param name="strSPName"></param>
        /// <param name="cmdTypeName"></param>
        /// <param name="arrlstParameters"></param>
        /// <returns></returns>
        public static SqlDataReader GenerateDataReader(string strSPName, CommandType cmdTypeName, SqlParameter[] pSQLParameters)
        {
            SqlDataReader objSqlDataReader = null;
            SqlConnection Conn = new SqlConnection(GetProviders());
            Conn.Open();
            SqlCommand objSQLCommand = new SqlCommand();
            objSQLCommand.CommandType = cmdTypeName;
            objSQLCommand.CommandText = strSPName;
            objSQLCommand.Connection = Conn;

            if (pSQLParameters != null)
            {
                foreach (SqlParameter param in pSQLParameters)
                {
                    objSQLCommand.Parameters.Add(param);
                }
            }
            objSqlDataReader = objSQLCommand.ExecuteReader(CommandBehavior.CloseConnection);
            return objSqlDataReader;
        }

        /// <summary>
        /// Executes a SQL statement or stored procedure that does not return any data through a cursor.
        /// </summary>
        /// <param name="strSPName"></param>
        /// <param name="cmdTypeName"></param>
        /// <param name="pSQLParameters"></param>
        /// <returns></returns>
        public static bool ExecuteNonQuery(string strSPName, CommandType cmdTypeName, SqlParameter[] pSQLParameters)
        {
            bool ReturnStatus = false;
            SqlCommand objSQLCommand = new SqlCommand();
            SqlConnection Conn = null;
            try
            {
                Int32 NoOfRecords = 0;
                Conn = new SqlConnection(GetProviders());
                Conn.Open();
                objSQLCommand.CommandType = cmdTypeName;
                objSQLCommand.CommandText = strSPName;
                objSQLCommand.Connection = Conn;
                objSQLCommand.CommandTimeout = 0;
                int i = 0;
                if (pSQLParameters != null)
                {
                    foreach (SqlParameter param in pSQLParameters)
                    {
                        //if(param.Value==null)
                        //    objSQLCommand.Parameters.Add(DBNull.Value);
                        objSQLCommand.Parameters.Add(param);
                        i++;
                    }
                }
                NoOfRecords = objSQLCommand.ExecuteNonQuery();

                if (NoOfRecords > 0)
                {
                    ReturnStatus = true;
                }
                else
                {
                    ReturnStatus = false;
                }
            }
            finally
            {
                if (objSQLCommand != null)
                {
                    objSQLCommand.Dispose();
                }
                if (Conn != null)
                {
                    if (Conn.State == ConnectionState.Open)
                    {
                        Conn.Dispose();
                    }
                }
            }
            return ReturnStatus;
        }
        /// <summary>
        /// Executes a SQL statement or stored procedure that does not return any data through a cursor.
        /// </summary>
        /// <param name="strSPName"></param>
        /// <param name="cmdTypeName"></param>
        /// <param name="pSQLParameters"></param>
        /// <returns></returns>
        public static int ExecuteNonQueryInt(string strSPName, CommandType cmdTypeName, SqlParameter[] pSQLParameters)
        {
            int ReturnValue = 0;
            SqlCommand objSQLCommand = new SqlCommand();
            SqlConnection Conn = null;
            try
            {
                Conn = new SqlConnection(GetProviders());
                Conn.Open();
                objSQLCommand.CommandType = cmdTypeName;
                objSQLCommand.CommandText = strSPName;
                objSQLCommand.Connection = Conn;
                int i = 0;
                if (pSQLParameters != null)
                {
                    foreach (SqlParameter param in pSQLParameters)
                    {
                        objSQLCommand.Parameters.Add(param);
                        i++;
                    }
                }


                ReturnValue = objSQLCommand.ExecuteNonQuery();
            }
            finally
            {
                if (objSQLCommand != null)
                {
                    objSQLCommand.Dispose();
                }
                if (Conn != null)
                {
                    if (Conn.State == ConnectionState.Open)
                    {
                        Conn.Dispose();
                    }
                }
            }

            return ReturnValue;
        }
        public static int ExecuteNonQueryIntOutputReturn(string strSPName, CommandType cmdTypeName, SqlParameter[] pSQLParameters, string OutputParmName)
        {
            int ReturnValue;
            int sqlreturn;
            SqlCommand objSQLCommand = new SqlCommand();
            SqlConnection Conn = null;
            try
            {
                Conn = new SqlConnection(GetProviders());
                Conn.Open();
                objSQLCommand.CommandType = cmdTypeName;
                objSQLCommand.CommandText = strSPName;
                objSQLCommand.Connection = Conn;
                int i = 0;

                if (pSQLParameters != null)
                {
                    foreach (SqlParameter param in pSQLParameters)
                    {
                        objSQLCommand.Parameters.Add(param);
                        i++;
                    }
                }
                SqlParameter outputParam = new SqlParameter(OutputParmName, SqlDbType.BigInt);
                outputParam.Direction = ParameterDirection.Output;
                objSQLCommand.Parameters.Add(outputParam);
                sqlreturn = objSQLCommand.ExecuteNonQuery();
                if (sqlreturn == 1)
                {
                    ReturnValue = Convert.ToInt32(objSQLCommand.Parameters[OutputParmName].Value);
                    //  ReturnValue1 =Convert.ToInt64( outputParam.Value); 
                }
                else
                {
                    ReturnValue = 0;
                }

                //  ReturnValue = objSQLCommand.ExecuteNonQuery();
            }
            finally
            {
                if (objSQLCommand != null)
                {
                    objSQLCommand.Dispose();
                }
                if (Conn != null)
                {
                    if (Conn.State == ConnectionState.Open)
                    {
                        Conn.Dispose();
                    }
                }
            }

            return ReturnValue;
        }

		public static Int64 ExecuteNonQueryInt64OutputReturn(string strSPName, CommandType cmdTypeName, SqlParameter[] pSQLParameters, string OutputParmName)
		{
			Int64 ReturnValue;
			Int64 sqlreturn;
			SqlCommand objSQLCommand = new SqlCommand();
			SqlConnection Conn = null;
			try
			{
				Conn = new SqlConnection(GetProviders());
				Conn.Open();
				objSQLCommand.CommandType = cmdTypeName;
				objSQLCommand.CommandText = strSPName;
				objSQLCommand.Connection = Conn;
				int i = 0;

				if (pSQLParameters != null)
				{
					foreach (SqlParameter param in pSQLParameters)
					{
						objSQLCommand.Parameters.Add(param);
						i++;
					}
				}
				SqlParameter outputParam = new SqlParameter(OutputParmName, SqlDbType.BigInt);
				outputParam.Direction = ParameterDirection.Output;
				objSQLCommand.Parameters.Add(outputParam);
				sqlreturn = objSQLCommand.ExecuteNonQuery();
				if (sqlreturn == 1)
				{
					ReturnValue = Convert.ToInt32(objSQLCommand.Parameters[OutputParmName].Value);
					//  ReturnValue1 =Convert.ToInt64( outputParam.Value); 
				}
				else
				{
					ReturnValue = 0;
				}

				//  ReturnValue = objSQLCommand.ExecuteNonQuery();
			}
			finally
			{
				if (objSQLCommand != null)
				{
					objSQLCommand.Dispose();
				}
				if (Conn != null)
				{
					if (Conn.State == ConnectionState.Open)
					{
						Conn.Dispose();
					}
				}
			}

			return ReturnValue;
		}
		internal static DataSet GenerateDataSetcuteNonQuery(string p, CommandType commandType, SqlParameter[] param)
        {
            throw new NotImplementedException();
        }
        public static DataTable GenerateDataTableNoParms(string strSPName, CommandType cmdTypeName)
        {
            DataTable objDataTable = new DataTable();
            SqlDataAdapter objSqlDataAdapter = null;
            SqlConnection Conn = null;
            SqlCommand objSQLCommand = null;
            try
            {
                Conn = new SqlConnection(GetProviders());
                Conn.Open();
                objSQLCommand = new SqlCommand();
                objSQLCommand.CommandType = cmdTypeName;
                objSQLCommand.CommandText = strSPName;
                objSQLCommand.Connection = Conn;


                objSqlDataAdapter = new SqlDataAdapter(objSQLCommand);
                objSqlDataAdapter.Fill(objDataTable);

            }
            finally
            {
                if (objSqlDataAdapter != null)
                {
                    objSqlDataAdapter.Dispose();
                }
                if (objSQLCommand != null)
                {
                    objSQLCommand.Dispose();
                }
                if (Conn != null)
                {
                    if (Conn.State == ConnectionState.Open)
                    {
                        //ConnStr.Close();
                        Conn.Dispose();
                    }
                }

            }
            return objDataTable;
        }
        public static string ExecuteNonQueryIntOutputReturnString(string strSPName, CommandType cmdTypeName, SqlParameter[] pSQLParameters, string OutputParmName)
        {
            string ReturnValue;

            int sqlreturn;
            SqlCommand objSQLCommand = new SqlCommand();
            SqlConnection Conn = null;

            try
            {
                Conn = new SqlConnection(GetProviders());
                Conn.Open();
                objSQLCommand.CommandType = cmdTypeName;
                objSQLCommand.CommandText = strSPName;
                objSQLCommand.Connection = Conn;
                int i = 0;

                if (pSQLParameters != null)
                {
                    foreach (SqlParameter param in pSQLParameters)
                    {
                        objSQLCommand.Parameters.Add(param);
                        i++;
                    }
                }
                SqlParameter outputParam = new SqlParameter(OutputParmName, SqlDbType.VarChar, 255);
                outputParam.Direction = ParameterDirection.Output;
                objSQLCommand.Parameters.Add(outputParam);

                sqlreturn = objSQLCommand.ExecuteNonQuery();
                //if (sqlreturn == 1)
                //{
                ReturnValue = objSQLCommand.Parameters[OutputParmName].Value.ToString();
                //  ReturnValue1 =Convert.ToInt64( outputParam.Value); 
                //}
                //else
                //{
                //    ReturnValue = "Unable to save data";
                //}

                //  ReturnValue = objSQLCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ReturnValue = ex.Message;
            }
            finally
            {
                if (objSQLCommand != null)
                {
                    objSQLCommand.Dispose();
                }
                if (Conn != null)
                {
                    if (Conn.State == ConnectionState.Open)
                    {
                        Conn.Dispose();
                    }
                }
            }

            return ReturnValue;
        }
        public static string ExecuteNonQueryIntOutputReturnStringTransction(string strSPName, CommandType cmdTypeName, SqlParameter[] pSQLParameters, string OutputParmName)
        {
            string ReturnValue;

            int sqlreturn;
            SqlCommand objSQLCommand = new SqlCommand();
            SqlTransaction objTranction;
            SqlConnection Conn = null;
            Conn = new SqlConnection(GetProviders());
            if (Conn.State == ConnectionState.Closed)
            {
                Conn.Open();
            }
            objTranction = Conn.BeginTransaction(IsolationLevel.Serializable, "MumbaiMirror");
            try
            {




                objSQLCommand.CommandType = cmdTypeName;
                objSQLCommand.CommandText = strSPName;
                objSQLCommand.Connection = Conn;
                objSQLCommand.Transaction = objTranction;
                int i = 0;

                if (pSQLParameters != null)
                {
                    foreach (SqlParameter param in pSQLParameters)
                    {
                        objSQLCommand.Parameters.Add(param);
                        i++;
                    }
                }
                SqlParameter outputParam = new SqlParameter(OutputParmName, SqlDbType.VarChar, 255);
                outputParam.Direction = ParameterDirection.Output;
                objSQLCommand.Parameters.Add(outputParam);
                objSQLCommand.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                sqlreturn = objSQLCommand.ExecuteNonQuery();
                sqlreturn = Convert.ToInt32(objSQLCommand.Parameters["@ReturnValue"].Value);
                if (sqlreturn == 1)
                {
                    ReturnValue = objSQLCommand.Parameters[OutputParmName].Value.ToString();
                    // ReturnValue1 = Convert.ToInt64(outputParam.Value);
                    objTranction.Commit();
                }
                else
                {
                    ReturnValue = "Unable to save data";
                    objTranction.Rollback();
                }

                //  ReturnValue = objSQLCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ReturnValue = ex.Message;
                objTranction.Rollback();
            }
            finally
            {
                if (objSQLCommand != null)
                {
                    objSQLCommand.Dispose();
                }
                if (Conn != null)
                {
                    if (Conn.State == ConnectionState.Open)
                    {
                        Conn.Dispose();
                    }
                }
            }

            return ReturnValue;
        }

    }
}
